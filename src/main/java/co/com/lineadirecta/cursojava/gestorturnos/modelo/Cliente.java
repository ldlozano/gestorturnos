package co.com.lineadirecta.cursojava.gestorturnos.modelo;


/**
 * Clase que representa un cliente que llega para ser atendido
 * @author Lenin Lozano
 */
public class Cliente {
    private String nombre;
    private String movil;

    public Cliente(String nombre, String movil) {
        this.nombre = nombre;
        this.movil = movil;
    }

    public String getNombre() {
        return nombre;
    }

    public String getMovil() {
        return movil;
    }
}
