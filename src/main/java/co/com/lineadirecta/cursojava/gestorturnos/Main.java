package co.com.lineadirecta.cursojava.gestorturnos;

import co.com.lineadirecta.cursojava.gestorturnos.modelo.Cliente;
import co.com.lineadirecta.cursojava.gestorturnos.modelo.GestorTurno;
import co.com.lineadirecta.cursojava.gestorturnos.modelo.Turno;

import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String [] args) throws InterruptedException {
        Cliente cliente = new Cliente("Lenin", "315236322");
        Cliente cliente1 = new Cliente("Luis", "315236322");
        Cliente cliente2 = new Cliente("Pedro", "315236322");

        GestorTurno gestorTurno = new GestorTurno();
        gestorTurno.recibirCliente(cliente);
        System.out.println("Tiempo Esperar:" + gestorTurno.calcularTiempoMedioEspera());
        TimeUnit.SECONDS.sleep(10);
        gestorTurno.recibirCliente(cliente1);
        System.out.println("Tiempo Esperar:" + gestorTurno.calcularTiempoMedioEspera());
        TimeUnit.SECONDS.sleep(10);
        gestorTurno.recibirCliente(cliente2);
        System.out.println("Tiempo Esperar:" + gestorTurno.calcularTiempoMedioEspera());
        TimeUnit.SECONDS.sleep(40);
        System.out.println("Tiempo Esperar:" + gestorTurno.calcularTiempoMedioEspera());

        Turno turnoAtendido1 = gestorTurno.atenderCliente();
        TimeUnit.SECONDS.sleep(15);
        Turno turnoAtendido2 = gestorTurno.atenderCliente();
        TimeUnit.SECONDS.sleep(15);
        Turno turnoAtendido3 = gestorTurno.atenderCliente();

        gestorTurno.terminarAtencion(turnoAtendido1);
        gestorTurno.terminarAtencion(turnoAtendido2);
        gestorTurno.terminarAtencion(turnoAtendido3);


    }


}
