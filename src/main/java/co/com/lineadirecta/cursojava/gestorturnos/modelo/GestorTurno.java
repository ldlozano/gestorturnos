package co.com.lineadirecta.cursojava.gestorturnos.modelo;

import java.util.*;

public class GestorTurno {
    private Queue<Turno> colaEspera;
    private List<Turno> colaAtencion;

    public GestorTurno() {
        colaAtencion = new ArrayList<>(20);
        colaEspera = new LinkedList<>();
    }

    public void recibirCliente(Cliente cliente){
        Turno turno = new Turno(cliente);
        colaEspera.add(turno);

    }

    public Turno atenderCliente(){
        Turno turno = colaEspera.remove();
        turno.atender();
        colaAtencion.add(turno);
        return turno;
    }

    public void terminarAtencion(Turno turno){
        int posicion = colaAtencion.indexOf(turno);
        Turno turnoAtendido = colaAtencion.remove(posicion);
        turnoAtendido.terminarAtencion();
    }

    public float calcularTiempoMedioEspera() {
        long tiempo = 0;
        for (Turno t: colaEspera) {
            tiempo = tiempo + t.tiempoEspera();
        }
        if(colaEspera.size() > 0){
            return tiempo / colaEspera.size();
        }else {
            return 0;
        }
    }
}
