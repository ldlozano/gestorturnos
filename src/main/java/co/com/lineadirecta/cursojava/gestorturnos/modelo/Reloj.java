package co.com.lineadirecta.cursojava.gestorturnos.modelo;

import java.util.Calendar;

public class Reloj {

    private  static  Reloj instancia;



    private Reloj(){

    }

    public long horaActual(){
        return Calendar.getInstance().getTimeInMillis();
    }

    public static Reloj getInstance(){
        if( instancia == null) {
            instancia = new Reloj();
        }
        return instancia;
    }

}
