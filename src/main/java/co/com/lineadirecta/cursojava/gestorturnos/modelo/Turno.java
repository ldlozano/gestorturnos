package co.com.lineadirecta.cursojava.gestorturnos.modelo;

import java.util.Objects;

public class Turno {
    private Cliente cliente;
    private long horaLlegada;
    private long horaAtencion;
    private long horaSalida;


    public Turno(Cliente cliente) {
        this.cliente = cliente;
        this.horaLlegada = Reloj.getInstance().horaActual();
    }

    public long tiempoAtencion(){
        return horaSalida - horaAtencion;
    }

    public long tiempoEspera(){
        if(horaAtencion == 0){
            return Reloj.getInstance().horaActual() - horaLlegada;
        }else{
            return horaAtencion - horaLlegada;
        }
    }

    public void atender(){
        horaAtencion = Reloj.getInstance().horaActual();
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void terminarAtencion() {
        this.horaSalida = Reloj.getInstance().horaActual();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Turno turno = (Turno) o;
        return Objects.equals(cliente, turno.cliente);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cliente);
    }
}
